package org.turko.test.hw13.frames;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.turko.test.base.BaseTestClassForPractice;

public class hw13 extends BaseTestClassForPractice {

    @BeforeMethod
    public void beforeMethod() {
        driver.get("http://the-internet.herokuapp.com/nested_frames");
    }

    @DataProvider(name = "dataProvider")
    public Object[][] dataProvider() {
        return new Object[][]{
                {"frame-left", "LEFT"},
                {"frame-middle", "MIDDLE"},
                {"frame-right", "RIGHT"},
                {"frame-bottom", "BOTTOM"},
        };
    }

    @Test(dataProvider = "dataProvider")
    public void frameName(String Name, String Expected) {

        if (!Name.equals("frame-bottom")) {
            driver.switchTo().frame("frame-top");
        }
        driver.switchTo().frame(Name);

        WebElement frameText = driver.findElement(By.tagName("body"));
        String text = frameText.getText().trim();
        Assert.assertEquals(text, Expected);

        driver.switchTo().defaultContent();
    }
}
